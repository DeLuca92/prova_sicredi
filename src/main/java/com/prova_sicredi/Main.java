package com.prova_sicredi;


import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.prova_sicredi.tasks.DeletarUsuarioTask;
import com.prova_sicredi.tasks.InserirDadosTask;
import com.prova_sicredi.tasks.PopUpPageTask;
import com.prova_sicredi.tasks.SelecionarVersaoTask;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Main {

	private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    //@Test
    public void Desafio1() throws InterruptedException  {
    	driver.get("https://www.grocerycrud.com/demo/bootstrap_theme/");
    	SelecionarVersaoTask selecionar = new SelecionarVersaoTask(driver);
    	selecionar.AcaoTela1("Bootstrap V4 Theme");
    	Thread.sleep(350);
    	InserirDadosTask insert = new InserirDadosTask(driver);
    	insert.AcaoTela2();	
    	assertEquals("Verificando Mensagem","Your data has been successfully stored into the database. Edit Customer or Go back to list", insert.getMsg());
    }
    
    @Test
    public void Desafio2() throws InterruptedException  {
    	Desafio1();
    	InserirDadosTask insert = new InserirDadosTask(driver);
    	insert.Voltar();
    	DeletarUsuarioTask del = new DeletarUsuarioTask(driver);
    	del.AcaoDeletarParte1();
    	PopUpPageTask pop = new PopUpPageTask(driver);
    	String msgForms = "Are you sure that you want to delete this 1 item?";
		assertEquals("Verificando Mensagem PopUp", msgForms, pop.getMsgForms());
    	pop.clicarDeletar();
		String msgDelete = "Your data has been successfully deleted from the database.";
		assertEquals("Verificando mensagem de deletado com sucesso", msgDelete, del.getMensagemSucessocoDeletado());
    	
    }
    
       

}