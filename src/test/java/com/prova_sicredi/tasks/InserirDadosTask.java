package com.prova_sicredi.tasks;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.prova_sicredi.objects.*;



public class InserirDadosTask {

	
	InserirDadosObjects inserirObj;
	
	public InserirDadosTask(WebDriver driver) {
		inserirObj = new InserirDadosObjects(driver);
	}
	
	
	public void AcaoTela2() {
		inserirObj.PrimeiroNome().sendKeys("Teste Sicredi");
		inserirObj.UltimoNome().sendKeys("Teste");
		inserirObj.contactNome().sendKeys("Daniel Francisco De Luca");
		inserirObj.Telefone().sendKeys("51 9999-9999");
		inserirObj.Endereco1().sendKeys("Av Assis Brasil, 3970");
		inserirObj.Endereco2().sendKeys("Torre D");
		inserirObj.Cidade().sendKeys("Porto Alegre");
		inserirObj.Estado().sendKeys("RS");
		inserirObj.CodPostal().sendKeys("91000-000");
		inserirObj.Pais().sendKeys("Brasil");
		inserirObj.FromEmParte1().click();
		inserirObj.FromEmParte2().sendKeys("Fixter");
		inserirObj.FromEmParte3().sendKeys(Keys.ENTER);
		inserirObj.LimiteCredito().sendKeys("200");
		inserirObj.Salvar().click();
		//Assert.assertEquals("2Your data has been successfully stored into the database. Edit Customer or Go back to list", inserirObj.Verificar().getText());
	}
	
	public String getMsg() {
		return inserirObj.Verificar().getText();
	}
	
	public void Voltar() {
		inserirObj.Voltar().click();
	}
	
}
