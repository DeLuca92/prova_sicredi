package com.prova_sicredi.tasks;

import com.prova_sicredi.objects.*;
import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.WebDriver;
public class SelecionarVersaoTask {

	SelecionarVersaoObjects selecionar;
	
	public SelecionarVersaoTask(WebDriver driver) {
		selecionar = new SelecionarVersaoObjects(driver);
    }
    
	
	  public void AcaoTela1(String versao) {
		  
		  Select combobox = new Select(selecionar.selecionarTipo());
		  combobox.selectByVisibleText(versao);
		  selecionar.adicionarNovo().click();
	     
	    }
	
}
