package com.prova_sicredi.tasks;

import org.openqa.selenium.WebDriver;

import com.prova_sicredi.objects.PopUpPageObjects;


public class PopUpPageTask {

	PopUpPageObjects popUpObj;
	
	
	public PopUpPageTask(WebDriver driver) {
		popUpObj = new PopUpPageObjects(driver);
	}
	
	
	public String getMsgForms() {
		
		return popUpObj.TextoPopUp().getText();
		
	
	}
	
	public void clicarDeletar() {
		popUpObj.ClicarBtnDeletarPopUp().click();
	}
	
}
