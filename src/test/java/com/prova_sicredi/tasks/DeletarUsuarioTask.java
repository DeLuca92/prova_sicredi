package com.prova_sicredi.tasks;

import org.openqa.selenium.WebDriver;

import com.prova_sicredi.objects.DeletarUsuarioObjects;


public class DeletarUsuarioTask {

	DeletarUsuarioObjects deletarObj;
	
	public DeletarUsuarioTask(WebDriver driver) {
		deletarObj = new DeletarUsuarioObjects(driver);
	}
	
	
	public void AcaoDeletarParte1() throws InterruptedException {
		deletarObj.ClicarProcurarNome().click();
		deletarObj.ClicarCampoNome().click();
		deletarObj.InsertName().sendKeys("Teste Sicredi");
		Thread.sleep(10000);
		deletarObj.ClicarCheckBox().click();
		deletarObj.ClicarDeletarTela().click();
	}
	
	
	public String  getMensagemSucessocoDeletado() {
		return deletarObj.getMsgCerto().getText();
		//Assert.assertEquals("Verificando mensagem de deletado com sucesso", msg, );
		
	}
	
	
}
