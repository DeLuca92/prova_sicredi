package com.prova_sicredi.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeletarUsuarioObjects {

	private WebDriver driver;
	

	    public DeletarUsuarioObjects(WebDriver driver){
	        this.driver = driver;
	    }
	
	    public WebElement ClicarProcurarNome() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[1]/th[2]")));
	    }
		public WebElement ClicarCampoNome() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]/input")));
			//return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]"));
		}
		public WebElement InsertName() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[3]/input")));
			//return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form
		}
	    
		public WebElement ClicarCheckBox() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[1]/div/input")));
			//return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[1]/div/input"));
		}
	 
		public WebElement ClicarDeletarTela() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[2]/div[1]/a")));
			//return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[2]/div[1]/a"));
		}
		
		
		public WebElement getMsgCerto() {
			WebDriverWait wait = new WebDriverWait(driver, 20);
	        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[3]/span[3]/p")));
			//return driver.findElement(By.xpath(""));

			
		}
		
		
	   
}
