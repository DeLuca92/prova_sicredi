package com.prova_sicredi.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PopUpPageObjects {

	private WebDriver driver;
	

    public PopUpPageObjects(WebDriver driver){
        this.driver = driver;
    }
    
    public WebElement TextoPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]")));
    }
	public WebElement ClicarBtnDeletarPopUp() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]")));
		//return driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]"));
	}
    
    
    

    		

	
	
}
