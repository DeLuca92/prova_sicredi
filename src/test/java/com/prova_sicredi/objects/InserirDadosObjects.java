package com.prova_sicredi.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InserirDadosObjects {

	
	  private WebDriver driver;

	    public InserirDadosObjects(WebDriver driver){
	        this.driver = driver;
	    }
	    
	    
	public WebElement PrimeiroNome() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-customerName\"]")));
    }
	public WebElement UltimoNome() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-contactLastName\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-contactLastName\"]"));
	}
	public WebElement contactNome() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-contactFirstName\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-contactFirstName\"]"));

	}
	public WebElement Telefone() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-phone\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-phone\"]"));

	}
	public WebElement Endereco1() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
       return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-addressLine1\"]")));

		//return driver.findElement(By.xpath("//*[@id=\"field-addressLine1\"]"));

	}
	public WebElement Endereco2() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-addressLine2\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-addressLine2\"]"));

	}
	
	public WebElement Cidade() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-city\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-city\"]"));

	}

	public WebElement Estado() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-state\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-state\"]"));

	}
	
	public WebElement CodPostal() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-postalCode\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-postalCode\"]"));

	}
	
	public WebElement Pais() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-country\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-country\"]"));

	}
	
	public WebElement FromEmParte1() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]"));
	}
	public WebElement FromEmParte2() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input")));
		//return driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input"));
	}
	public WebElement FromEmParte3() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input")));
		//return driver.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/div/input"));
	}
	
	
	
	public WebElement LimiteCredito() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"field-creditLimit\"]")));
		//return driver.findElement(By.xpath("//*[@id=\"field-creditLimit\"]"));

	}
	
	
	public WebElement Salvar() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"form-button-save\"]")));

		//return driver.findElement(By.xpath("//*[@id=\"form-button-save\"]"));

	}
	
	public WebElement Verificar() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"report-success\"]/p")));
    }
	
	
	
	public WebElement Voltar() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"save-and-go-back-button\"]")));


		//return driver.findElement(By.xpath("//*[@id=\"save-and-go-back-button\"]"));

	}
	
	
	
	
	
	}
	
	
	
	
	
	
	
	    

