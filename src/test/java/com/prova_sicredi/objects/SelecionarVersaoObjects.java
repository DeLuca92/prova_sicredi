package com.prova_sicredi.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SelecionarVersaoObjects {

    private WebDriver driver;
    WebDriverWait wait;

    public SelecionarVersaoObjects(WebDriver driver){
        this.driver = driver;
       // wait = new WebDriverWait(driver, 20);
    }
    
    public WebElement selecionarTipo() {
    	//WebDriverWait wait = new WebDriverWait(driver, 20);
    	return driver.findElement(By.xpath("//*[@id=\"switch-version-select\"]"));
        //return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"switch-version-select\"]")));
    }
	
    public WebElement adicionarNovo() {
    	//WebDriverWait wait = new WebDriverWait(driver, 20);
      //  return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")));
    	return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a"));

    	//return driver.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a"));
    }
	
}
